export const environment = {
  production: false,
  appVersion: 'v1.0.0',
  apiUrl: 'http://localhost:3000/api/',
  appThemeName: 'TBC Bank Payme Test App',
};

