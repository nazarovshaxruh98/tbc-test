import {Component} from '@angular/core';
import {TodosTableComponent} from "./components/todos-table/todos-table.component";

@Component({
  selector: 'app-todos',
  standalone: true,
  imports: [
    TodosTableComponent
  ],
  templateUrl: './todos.component.html',
  styleUrl: './todos.component.scss'
})
export class TodosComponent {
}
