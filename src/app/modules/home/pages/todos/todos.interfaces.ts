export interface ITodos {
  "id": string,
  "title": string,
  "completed"?: boolean,
  "created_at"?: Date,
  "updated_at"?: Date,
  "user": number | string
}
