import {Component, inject} from '@angular/core';
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {LocalStorageService} from "../../../../../../core/services/local-storage.service";
import {ButtonComponent} from "../../../../../../shared/form/widgets/button/button.component";
import {InputComponent} from "../../../../../../shared/form/widgets/input/input.component";
import {TodosTableService} from "../todos-table/todos-table.service";
import {ITodos} from "../../todos.interfaces";

@Component({
  selector: 'app-add-todo',
  standalone: true,
  imports: [
    ButtonComponent,
    InputComponent,
    ReactiveFormsModule
  ],
  templateUrl: './add-todo.component.html',
  styleUrl: './add-todo.component.scss'
})
export class AddTodoComponent {
  // DEPENDENCY INJECTS
  _localStorageService = inject(LocalStorageService)
  _todosTableService = inject(TodosTableService)

  // SATE
  form = new FormGroup({
    title: new FormControl('', [Validators.required]),
    completed: new FormControl(false),
    user: new FormControl(this._localStorageService.userId)
  });

  // METHODS
  submit() {
    this._todosTableService.insert(this.form.value as ITodos).subscribe()
  }
}
