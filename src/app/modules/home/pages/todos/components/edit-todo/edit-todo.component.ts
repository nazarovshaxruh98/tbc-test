import {Component, inject, OnInit, signal} from '@angular/core';
import {ButtonComponent} from "../../../../../../shared/form/widgets/button/button.component";
import {InputComponent} from "../../../../../../shared/form/widgets/input/input.component";
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {LocalStorageService} from "../../../../../../core/services/local-storage.service";
import {TodosTableService} from "../todos-table/todos-table.service";
import {ITodos} from "../../todos.interfaces";
import {catchError, finalize, tap, throwError} from "rxjs";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-edit-todo',
  standalone: true,
  imports: [
    ButtonComponent,
    InputComponent,
    ReactiveFormsModule
  ],
  templateUrl: './edit-todo.component.html',
  styleUrl: './edit-todo.component.scss'
})
export class EditTodoComponent implements OnInit {
  // DEPENDENCY INJECTS
  _localStorageService = inject(LocalStorageService)
  _todosTableService = inject(TodosTableService)

  // STATE
  isGettingData = signal(false)

  form = new FormGroup({
    id: new FormControl(''),
    title: new FormControl('', [Validators.required]),
    completed: new FormControl(false),
    user: new FormControl(this._localStorageService.userId)
  });

  // LIFECYCLES
  ngOnInit() {
    this.getTodo()
  }

  // Methods
  getTodo() {
    this.isGettingData.set(true)
    const id = this._todosTableService.selectedRow().id
    this._todosTableService.getOne<ITodos>(id).pipe(tap(data => {
        this.form.setValue({
          id: data.id,
          user: data.user,
          title: data.title,
          completed: data.completed as boolean
        })
      }), catchError((error: HttpErrorResponse) => {
        this._todosTableService._modalService.close()
        return throwError(() => error);
      }),
      finalize(() => {
        this.isGettingData.set(false)
      })
    ).subscribe()
  }

  submit() {
    this._todosTableService.update(this.form.value as ITodos).subscribe()
  }
}
