import {Injectable} from '@angular/core';
import {BaseTableLoadService} from "../../../../../../core/services/base-table-load.service";
import {ITodos} from "../../todos.interfaces";
import {ColDef} from "ag-grid-community";
import {stringToDate} from "../../../../../../core/utils/dateHandler";
import {ActionsComponent} from "../../../../../../shared/table/base-table-load/widgets/actions.component";
import {AddTodoComponent} from "../add-todo/add-todo.component";
import {EditTodoComponent} from "../edit-todo/edit-todo.component";

@Injectable({
  providedIn: 'root'
})
export class TodosTableService extends BaseTableLoadService<ITodos> {
  colDefs: ColDef[] = [
    {
      headerName: "ID",
      field: "id",
    },
    {
      headerName: "Title",
      field: "title",
    },
    {
      headerName: "Completed",
      field: "completed",
      cellRenderer: 'agCheckboxCellRenderer',
      cellEditor: 'agCheckboxCellEditor',
    },
    {
      headerName: "Created",
      field: "created_at",
      cellRenderer: (params: any) => stringToDate(params.value)
    },
    {
      headerName: "Updated",
      field: "updated_at",
      cellRenderer: (params: any) => stringToDate(params.value)
    },
    {
      headerName: "Actions",
      field: "actions",
      cellRenderer: ActionsComponent,
      cellRendererParams: {
        handleDelete: this.onDelete.bind(this),
        handleEdit: this.editItem.bind(this),
      },
      pinned: "right",
    },
  ]


  getUrl(): string {
    return "todo/";
  }

  onDelete(data: ITodos) {
    this._confirmationDialogService.confirm('Confirm', 'Do you really want to delete ?')
      .then(() => this.delete(data.id).subscribe(
        () => {
          this.toastrService.success("Item deleted successfully")
        }
      ))
      .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
    this.selectedRow.set(data);
  }

  addItem() {
    this._modalService.open({
      component: AddTodoComponent,
      title: "Add To Do",
    });
  }

  editItem(data: ITodos) {
    this.selectedRow.set(data);
    this._modalService.open({
      component: EditTodoComponent,
      title: "Edit To Do",
    });
  }
}
