import {Component, inject} from '@angular/core';
import {BaseTableLoadComponent} from "../../../../../../shared/table/base-table-load/base-table-load.component";
import {TodosTableService} from "./todos-table.service";

@Component({
  selector: 'app-todos-table',
  standalone: true,
  imports: [
    BaseTableLoadComponent
  ],
  templateUrl: './todos-table.component.html',
  styleUrl: './todos-table.component.scss'
})
export class TodosTableComponent {
  // DEPENDENCY INJECTS
  _todosTableService = inject(TodosTableService)
}
