import {Route} from "@angular/router";

export const homeRoutes: Route[] = [
  {
    path: '',
    loadComponent: () =>
      import("./home.component").then((c) => c.HomeComponent),
    children: [
      {path: '', pathMatch: 'full', redirectTo: 'todos'},
      {
        path: 'todos',
        loadComponent: () => import('./pages/todos/todos.component').then((c) => c.TodosComponent)
      },
    ]
  },
]
