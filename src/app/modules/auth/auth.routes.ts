import {Route} from "@angular/router";
import {LOGIN_PAGE_PATH} from "../../core/constants/paths";

export const authRoutes: Route = {
  path: 'auth',
  loadComponent: () =>
    import("./auth.component").then((c) => c.AuthComponent),
  children: [
    {
      path: "login",
      loadComponent: () =>
        import("./pages/login/login.component").then(
          (c) => c.LoginComponent
        ),
    },
    {path: '', redirectTo: LOGIN_PAGE_PATH, pathMatch: 'full'},
  ]

}
