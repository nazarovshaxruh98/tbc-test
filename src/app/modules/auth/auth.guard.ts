import {inject, Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Router, RouterStateSnapshot} from "@angular/router";
import {LocalStorageService} from "../../core/services/local-storage.service";
import {AUTH_PAGE_PATH} from "../../core/constants/paths";

@Injectable({providedIn: "root"})
export class AuthGuard {
  _localStorageService = inject(LocalStorageService)
  _router = inject(Router)

  // eslint-disable-next-line
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const accessToken = this._localStorageService.accessToken;

    if (accessToken) {
      return true;
    }

    this._router.navigate([AUTH_PAGE_PATH]);
    return false;
  }
}
