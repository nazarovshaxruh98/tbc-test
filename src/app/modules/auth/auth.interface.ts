export interface AuthLogin {
  email: string;
  password: string;
}

export interface AuthLoginResponse {
  token: string,
  username: string,
  user_id: number | string
}
