import {inject, Injectable, signal} from '@angular/core';
import {catchError, finalize, map, Observable, throwError} from "rxjs";
import {AuthLogin, AuthLoginResponse} from "./auth.interface";
import {environment} from "../../../environments/environment";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {LocalStorageService} from "../../core/services/local-storage.service";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {AUTH_PAGE_PATH, LOGIN_PAGE_PATH} from "../../core/constants/paths";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  // DEPENDENCY INJECTS
  _router = inject(Router)
  _http = inject(HttpClient)
  _localStorageService = inject(LocalStorageService)

  // STATE
  apiUrl = environment.apiUrl;
  isLoading = signal<boolean>(false);

  // METHODS
  login(data: AuthLogin): Observable<any> {
    this.isLoading.set(true);
    return this._http
      .post<AuthLoginResponse>(`${this.apiUrl}auth/token/login/`, data, {withCredentials: true})
      .pipe(
        map((userData: AuthLoginResponse) => {
          console.log(userData)

          this._localStorageService.setUserDataToLocalStorage(userData)
          this._router.navigate([''])
        }),
        catchError((error: HttpErrorResponse) => {

          return throwError(() => error)
        }),
        finalize(() => this.isLoading.set(false))
      )
      ;
  }

  logOut() {
    this._localStorageService.removeUserDataFromLocalStorage()
    this._router.navigate([AUTH_PAGE_PATH]);
  }
}
