import {Component, inject} from '@angular/core';
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {InputComponent} from "../../../../shared/form/widgets/input/input.component";
import {AuthService} from "../../auth.service";
import {AuthLogin} from "../../auth.interface";
import {ButtonComponent} from "../../../../shared/form/widgets/button/button.component";

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    InputComponent,
    ButtonComponent
  ],
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  _authService = inject(AuthService)

  loginForm = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
    password: new FormControl('', [Validators.required])
  });

  submit() {
    this._authService.login(this.loginForm.value as AuthLogin).subscribe()
  }
}
