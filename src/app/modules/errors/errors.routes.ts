import {Route} from "@angular/router";
export const errorsRoutes: Route = {
  path: 'error-404',
  loadComponent: () =>
    import("./errors.component").then((c) => c.ErrorsComponent),
}
