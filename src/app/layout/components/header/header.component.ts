import {Component, inject} from '@angular/core';
import {LocalStorageService} from "../../../core/services/local-storage.service";

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [],
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss'
})
export class HeaderComponent {
  _localStorageService = inject(LocalStorageService)

  getUserName() {
    return this._localStorageService.userName
  }
}
