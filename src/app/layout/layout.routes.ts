import {Route} from "@angular/router";
import {AuthGuard} from "../modules/auth/auth.guard";
import {homeRoutes} from "../modules/home/home.routes";

export const layoutRoutes: Route = {
  path: '',
  loadComponent: () =>
    import("./layout.component").then((c) => c.LayoutComponent),
  canActivate: [AuthGuard],
  children: homeRoutes
}
