export interface ITableDataResult<T = any> {
  count: number
  next: number | string | null
  previous: number | string | null
  results: Array<T>;
}

