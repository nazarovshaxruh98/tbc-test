export enum LocalStorageKeys {
  ACCESS_TOKEN = "ACCESS_TOKEN",
  USER_ID = "USER_ID",
  USER_NAME = "USER_NAME",
}
