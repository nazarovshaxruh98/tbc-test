import {inject, Injectable} from "@angular/core";
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {catchError, Observable, throwError} from "rxjs";
import {LocalStorageService} from "../services/local-storage.service";
import {RequestHeader} from "../enums/request-header";
import {ToastrService} from "ngx-toastr";
import {AuthService} from "../../modules/auth/auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  private authService = inject(AuthService);
  private toastrService = inject(ToastrService);
  private localStorageService = inject(LocalStorageService);

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const accessToken = this.localStorageService.accessToken;

    // Check if accessToken is available
    if (accessToken) {
      const modifiedHeaders = request.headers.set(RequestHeader.AUTHORIZATION, `Token ${accessToken}`);

      const modifiedRequest = request.clone({
        headers: modifiedHeaders,
      });

      return next.handle(modifiedRequest).pipe(
        catchError((error: HttpErrorResponse) => {
          this.handleError(error);
          return throwError(() => error);
        })
      );
    } else {
      return next.handle(request).pipe(
        catchError((error: HttpErrorResponse) => {
          this.handleError(error);
          return throwError(() => error);
        })
      );
    }
  }

  private handleError(error: HttpErrorResponse) {
    if (error.status === 400) {
      this.toastrService.error(
        error.message ?? "Bad Request"
      );
    }
    if (error.status === 404) {
      this.toastrService.error(
        error.message ?? "Not Found"
      );
    }
    if (error.status === 500) {
      this.toastrService.error("Server Error");
    }
    if (error.status === 409) {
      this.toastrService.error(
        error.message ?? "Conflict with the current state of a resource"
      );
    }
    if (error.status === 0) {
      this.toastrService.error(
        error.message ?? "No response"
      );
    }
    if (error.status === 401) {
      this.authService.logOut();
    }
  }
}
