export const stringToDate = (text: Date): any => {
  if (text == null) return null;
  text = new Date(text);

  return `${text.getFullYear()}-${text.getMonth()}-${text.getDay()}`;
};
