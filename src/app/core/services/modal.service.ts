/*eslint-disable */
import {Injectable} from "@angular/core";
import {NavigationStart, Router} from "@angular/router";
import {NgbModal, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";
import {ModalFormComponent} from "../../shared/modal/modal-form/modal-form.component";
import {IModalService} from "../../shared/modal/modal-form/modal-form.interface";

@Injectable({
  providedIn: "root",
})
export class FormModalService {
  constructor(
    private modalService: NgbModal,
    private router: Router
  ) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.closeAll();
      }
    });
  }

  private modalRef!: NgbModalRef;

  open(options: IModalService): Promise<boolean> {
    const {
      component,
      modalOptions = {},
      title,
      componentOptions = {},
    } = options;

    const defModalOptions = {centered: true, size: "md"};

    this.modalRef = this.modalService.open(ModalFormComponent, {
      ...defModalOptions,
      ...modalOptions,
    });
    this.modalRef.componentInstance.data = {
      component,
      componentOptions,
      modalOptions,
      title,
    };

    return this.modalRef.result;
  }

  close() {
    if (this.modalRef) this.modalRef.close(true);
  }

  closeAll() {
    this.modalService.dismissAll();
  }
}
