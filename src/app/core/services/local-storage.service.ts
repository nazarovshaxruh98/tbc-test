import {Injectable} from '@angular/core';
import {LocalStorageKeys} from "../enums/local-storage-keys";
import {AuthLoginResponse} from "../../modules/auth/auth.interface";

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  get userName() {
    return this.parseData(localStorage.getItem(LocalStorageKeys.USER_NAME));
  }

  set userName(name: string | null) {
    localStorage.setItem(LocalStorageKeys.USER_NAME, JSON.stringify(name));
  }

  get accessToken() {
    return this.parseData(localStorage.getItem(LocalStorageKeys.ACCESS_TOKEN));
  }

  set accessToken(token: string | null) {
    localStorage.setItem(LocalStorageKeys.ACCESS_TOKEN, JSON.stringify(token));
  }

  get userId() {
    return this.parseData(localStorage.getItem(LocalStorageKeys.USER_ID));
  }

  set userId(userId: string | number | null) {
    localStorage.setItem(LocalStorageKeys.USER_ID, JSON.stringify(userId));
  }


  setUserDataToLocalStorage({user_id, token, username}: AuthLoginResponse) {
    this.accessToken = token
    this.userId = user_id
    this.userName = username
  }

  removeUserDataFromLocalStorage() {
    localStorage.removeItem(LocalStorageKeys.ACCESS_TOKEN)
    localStorage.removeItem(LocalStorageKeys.ACCESS_TOKEN)
    localStorage.removeItem(LocalStorageKeys.USER_ID)
  }

  getUserDataFromLocalStorage() {
    return {
      accessToken: this.accessToken,
      userId: this.userId,
      userName: this.userName,
    }
  }

  parseData(data: any) {
    try {
      return JSON.parse(data);
    } catch (e) {
      return data;
    }
  }
}
