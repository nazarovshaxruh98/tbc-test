import {inject, Injectable, signal} from '@angular/core';
import {
  ColDef,
  GridApi,
  GridReadyEvent,
  IDatasource,
  IGetRowsParams,
  RowClassParams, RowStyle,
  ValueGetterParams
} from 'ag-grid-community';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {
  CustomLoadingOverlayComponent
} from "../../shared/table/base-table-load/widgets/custom-loading-overlay.component";
import {
  CustomNoRowsOverlayComponent
} from "../../shared/table/base-table-load/widgets/custom-no-rows-overlay.component";
import {ITableDataResult} from "../interfaces/base-table-load.interface";
import {catchError, finalize, Observable, tap, throwError} from "rxjs";
import {ConfirmationDialogService} from "../../shared/modal/confirmation-dialog/confirmation-dialog.service";
import {ToastrService} from "ngx-toastr";
import {error} from "@angular/compiler-cli/src/transformers/util";
import {FormModalService} from "./modal.service";

@Injectable({
  providedIn: 'root'
})
export abstract class BaseTableLoadService<T> {
  abstract colDefs: ColDef[];

  abstract getUrl(): string;

  // DEPENDENCY INJECTS
  protected _http = inject(HttpClient)
  protected _confirmationDialogService = inject(ConfirmationDialogService)
  protected toastrService = inject(ToastrService);
  public _modalService = inject(FormModalService);


  // STATE
  protected baseUrl = environment.apiUrl;

  private gridApi!: GridApi;


  readonly defaultColDef: ColDef = {
    resizable: true,
    sortable: false,
    filter: false,
    flex: 1,
    editable: false,
    cellDataType: false,
  };

  public rowData = signal<T[]>([]);
  public selectedRow = signal<any>(null);
  public isLoading = signal<boolean>(false);

  // Pagination state
  readonly pageSizeList: number[] = [10, 25, 50, 100];
  public pageSize = signal<number>(0);
  public currentPage = 1;
  public limit = signal<number>(10);
  public offset = signal<number>(0);


  //AgGrid Options
  gridOptions = {
    loadingOverlayComponent: CustomLoadingOverlayComponent,
    noRowsOverlayComponent: CustomNoRowsOverlayComponent,
    noRowsOverlayComponentParams: {
      refresh: this.reload.bind(this),
    },
    wrapHeaderText: true,
    autoHeaderHeight: true,
    suppressNoRowsOverlay: true,
  }

  getRowStyle(params: RowClassParams): RowStyle | undefined {
    if (params.data === undefined) {
      return {opacity: 0};
    }
    return {}
  }


  // Methods
  onGridReady(params: GridReadyEvent<T>) {
    this.gridApi = params.api;
    this.reload();
  }

  loadTableData() {
    return this._http.request<ITableDataResult<T>>("GET", this.baseUrl + this.getUrl(), {
      params: {
        limit: this.limit(),
        offset: this.offset()
      },
    })
  }


  dataSource: IDatasource = {
    getRows: (params: IGetRowsParams) => {
      const successCallback = (data: any, lastRow: number = 0) => {
        this.gridApi.hideOverlay();
        if (!lastRow) this.gridApi.showNoRowsOverlay();
        params.successCallback(data, lastRow);
      };

      this.gridApi.showLoadingOverlay();


      this
        .loadTableData()
        .subscribe(
          (tableDataResult: ITableDataResult<T>) => {
            const data = tableDataResult.results ?? []

            this.pageSize.set(tableDataResult.count)
            this.rowData.set(data);
            successCallback(data, data.length);

          },
          (err) => {
            this.rowData.set([]);
            successCallback([], 0);
          }
        );
    },
  };


  reload() {
    if (this.gridApi) {
      this.gridApi.setDatasource(this.dataSource);
      this.gridApi.refreshHeader();
    }
  }

  addNumberHeader() {
    const index = this.colDefs.findIndex(
      (colDef) => colDef.field == "headerNumber"
    );
    if (index !== -1) {
      this.colDefs.splice(index, 1);
    }
    const colDef: ColDef = {
      headerName: "No",
      field: "headerNumber",
      maxWidth: 80,
      valueGetter: (params: ValueGetterParams) => {
        return (
          Number(params.node?.rowIndex) +
          (this.currentPage - 1) * this.limit() +
          1
        );
      },
      filter: false,
      sortable: false,
      pinned: "left",
    };
    this.colDefs.unshift(colDef);
  }


  onPageSizeChange(size: number) {
    this.limit.set(size);
    this.offset.set((this.currentPage - 1) * this.limit())
    this.reload()
  }


  onPageChange(offset: number) {
    this.offset.set((offset - 1) * this.limit())
    this.reload()
  }

  delete(id: string) {
    this.gridApi.showLoadingOverlay();

    return this._http
      .delete(this.baseUrl + this.getUrl() + id + '/')
      .pipe(tap(() => this.reload()), catchError((error: HttpErrorResponse) => {
        this.gridApi.hideOverlay()
        return throwError(() => error);
      }));
  }

  insert(form: T) {
    this.isLoading.set(true)
    return this._http
      .post<T>(this.baseUrl + this.getUrl(), form)
      .pipe(
        tap(() => {
          this._modalService.close()
          this.reload()
        }),
        finalize(() => {
          this.isLoading.set(false)
        }));
  }

  update(form: T | any) {
    this.isLoading.set(true)

    return this._http
      .put(this.baseUrl + this.getUrl() + form.id + '/', form)
      .pipe(
        tap(() => {
          this._modalService.close()
          this.reload()
        }),
        finalize(() => {
          this.isLoading.set(false)
        }));
  }

  getOne<T>(id: string): Observable<T> {
    return this._http.get<T>(this.baseUrl + this.getUrl() + id + '/');
  }
}
