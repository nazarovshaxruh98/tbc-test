import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseTableLoadComponent } from './base-table-load.component';

describe('BaseTableLoadComponent', () => {
  let component: BaseTableLoadComponent;
  let fixture: ComponentFixture<BaseTableLoadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BaseTableLoadComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BaseTableLoadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
