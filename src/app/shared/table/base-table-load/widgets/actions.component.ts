/* eslint-disable */
import {ChangeDetectionStrategy, Component} from "@angular/core";
import {ICellRendererAngularComp} from "ag-grid-angular";

@Component({
  selector: "app-actions",
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div class="d-flex align-items-center gap-2">
      @if (this.isAction.edit) {
        <button (click)="onHandleEdit($event)" class="btn btn-outline-warning">
        <span>
          <i class="bi bi-pencil-square"></i>
        </span>
        </button>
      }
      @if (this.isAction.delete) {
        <button (click)="onHandleDelete($event)" class="btn btn-outline-danger py-1 px-2">
        <span>
            <i class="bi bi-trash"></i>
        </span>
        </button>
      }
    </div>`,
  styles: [
    `
      :host {
        width: 100%;
        display: inline-flex;
        justify-content: center;
      }
    `,
  ],
  standalone: true,
  imports: [],
})
export class ActionsComponent<T> implements ICellRendererAngularComp {
  params: any;
  isAction = {
    delete: true,
    edit: true,
  };

  agInit(params: any): void {
    this.params = params;
    this.isAction.delete = typeof params["handleDelete"] === "function";
    this.isAction.edit = typeof params["handleEdit"] === "function";
  }


  onHandleEdit(event: Event) {
    event.stopPropagation();
    if (this.params.handleEdit) this.params.handleEdit(this.params.data);
  }

  onHandleDelete(event: Event) {
    event.stopPropagation();
    if (this.params.handleDelete) this.params.handleDelete(this.params.data);
  }

  refresh() {
    return true;
  }
}
