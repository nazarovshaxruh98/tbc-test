import {Component} from "@angular/core";
import {INoRowsOverlayAngularComp} from "ag-grid-angular";
import {INoRowsOverlayParams} from "ag-grid-community";

@Component({
  selector: "app-no-rows-overlay",
  template: `
    <div class="ag-overlay-no-rows-wrapper">
      No Data
    </div>
    <button
      class="fs-2x text-success arrows-circle"
      (click)="refresh()"
    >Refresh
    </button
    >
  `,
  styles: [
    `
      .arrows-circle {
        cursor: pointer;
      }
    `,
  ],
  standalone: true,
})
export class CustomNoRowsOverlayComponent implements INoRowsOverlayAngularComp {
  public params!: INoRowsOverlayParams & { refresh: () => string };

  agInit(params: INoRowsOverlayParams & { refresh: () => string }): void {
    this.params = params;
  }

  refresh() {
    this.params.refresh();
  }
}
