import { Component } from "@angular/core";
import { ILoadingOverlayAngularComp } from "ag-grid-angular";
import { ILoadingOverlayParams } from "ag-grid-community";
import {NgOptimizedImage} from "@angular/common";

@Component({
  selector: "app-loading-overlay",
  template: `
    <div>
      <img ngSrc="../../../../../assets/media/spinner/green-spinner.svg" width="80" alt="spinner" height="80"/>
    </div>
  `,
  standalone: true,
  imports: [
    NgOptimizedImage
  ]
})
export class CustomLoadingOverlayComponent
  implements ILoadingOverlayAngularComp
{
  public params!: ILoadingOverlayParams & { loadingMessage: string };

  agInit(params: ILoadingOverlayParams & { loadingMessage: string }): void {
    this.params = params;
  }
}
