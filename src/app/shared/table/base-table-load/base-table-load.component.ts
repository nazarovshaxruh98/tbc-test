import {Component, Input, OnInit} from '@angular/core';
import {BaseTableLoadService} from "../../../core/services/base-table-load.service";

import {AgGridModule} from "ag-grid-angular";
import {NgbPagination} from "@ng-bootstrap/ng-bootstrap";
import {FormsModule} from "@angular/forms";
import {NgSelectModule} from "@ng-select/ng-select";

@Component({
  selector: 'app-base-table-load',
  standalone: true,
  imports: [
    AgGridModule,
    NgbPagination,
    FormsModule,
    NgSelectModule,
  ],
  templateUrl: './base-table-load.component.html',
  styleUrl: './base-table-load.component.scss'
})
export class BaseTableLoadComponent<T> implements OnInit {
  // @INPUTS
  @Input({required: true}) _defaultService!: BaseTableLoadService<T>;

  // LIFECYCLE
  ngOnInit() {
    this._defaultService.addNumberHeader();
  }

}
