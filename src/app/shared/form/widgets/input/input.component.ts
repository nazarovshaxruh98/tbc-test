import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormControl, ReactiveFormsModule, Validators} from '@angular/forms';

@Component({
  selector: 'app-input',
  standalone: true,
  imports: [
    ReactiveFormsModule
  ],
  templateUrl: './input.component.html',
  styleUrl: './input.component.scss'
})
export class InputComponent implements OnInit {
  @Input() customClass = "";
  @Input() placeholder = "";
  @Input() label = "";
  @Input() disabled = false;
  @Input() controlName: AbstractControl | string | null = "";
  @Input() type:
    | "text"
    | "number"
    | "password"
    | "email"
    | "date"
    | "search"
    | "time"
    | "hidden" = "text";


  required = false;
  formControl: FormControl = new FormControl();

  ngOnInit() {
    this.formControl =
      typeof this.controlName !== "string"
        ? (this.controlName as FormControl)
        : new FormControl(null);

    this.required = this.formControl.hasValidator(Validators.required);

    if (this.disabled) {
      this.formControl.disable();
    }
  }

  showValidationErrors() {
    return (
      this.formControl?.invalid &&
      (this.formControl?.touched || this.formControl?.dirty)
    );
  }
}
