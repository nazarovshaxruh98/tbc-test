import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NgClass} from "@angular/common";

@Component({
  selector: 'app-button',
  standalone: true,
  imports: [
    NgClass
  ],
  templateUrl: './button.component.html',
  styleUrl: './button.component.scss'
})
export class ButtonComponent {
  @Input() label = "";
  @Input() isPending = false;
  @Output() btnClick = new EventEmitter<void>();
  @Input() disabled = false;
  @Input() styleType:
    | "success"
    | "primary"
    | "secondary"
    | "danger"
    | "light"
    | "info"
    | "warning"
    | "dark"
    | "" = "success";


  onClick() {
    if (!this.disabled) {
      this.btnClick.emit();
    }
  }
}
