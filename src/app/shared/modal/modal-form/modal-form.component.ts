/* eslint-disable */
import {
  ChangeDetectionStrategy,
  Component,
  ComponentRef,
  Input,
  OnInit,
  ViewChild,
} from "@angular/core";

import { HostDirective } from "./host.directive";
import { NgbActiveModal, NgbModalModule } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-modal-form",
  templateUrl: "./modal-form.component.html",
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [HostDirective, NgbModalModule],
})
export class ModalFormComponent implements OnInit {
  @Input() data: any;
  @ViewChild(HostDirective, { static: true }) appHost!: HostDirective;

  constructor(public ngbActiveModal: NgbActiveModal) {}

  ngOnInit() {
    if (this.data && this.data.component) {
      this.loadComponent(this.data.component);
    }
  }

  close() {
    this.ngbActiveModal.close(false);
  }

  private loadComponent(component: any) {
    const _viewContainerRef = this.appHost.viewContainerRef;
    _viewContainerRef.clear();
    const componentRef: ComponentRef<any> =
      _viewContainerRef.createComponent<any>(component);
    for (const key in this.data?.componentOptions) {
      componentRef.instance[key] = this.data?.componentOptions[key];
    }
  }
}
