/*eslint-disable*/
import { NgbModalOptions } from "@ng-bootstrap/ng-bootstrap/modal/modal-config";

export interface IModalService {
  component: any;
  title: string;
  componentOptions?: any;
  modalOptions?: NgbModalOptions;
}
