import {Routes} from '@angular/router';
import {authRoutes} from "./modules/auth/auth.routes";
import {layoutRoutes} from "./layout/layout.routes";
import {errorsRoutes} from "./modules/errors/errors.routes";

export const routes: Routes = [
  layoutRoutes,
  authRoutes,
  errorsRoutes,
  {path: "**", redirectTo: 'error-404', pathMatch: "full"},
];
